/*name:        Snake_game.
description: It is simply a game which is used to play by the any person.
input:       person click on left, right, up, down button to change the direction of Snake.
output:      You get large length of snake to show your markes.
usage:       Use for intertanment by any person
example:     Snake_game*/

//generate a canvas variable
var cvs = document.getElementById("canvas")
//generate a variable for drwaing an element  
var ctx = cvs.getContext('2d')

//button function
var start_end=1;
function start_End(x) {
	start_end = +(x);
}
//function for draw_snake_shap
var snakeH = 10
var snakeW = 10

//variable which decide direction of snake
var dir = "right"
function draw_snake_shap(x,y) {
	ctx.fillStyle = "white"
	ctx.fillRect(x*snakeW,y*snakeH,snakeW,snakeH)
	ctx.fillStyle = "black"
	ctx.strokeRect(x*snakeW,y*snakeH,snakeW,snakeH)
}

//create snake
var len = 4
snake = []
for(var i = len-1;i>=0;i--) {
	snake.push({x:i,y:0})
}

//control direction
document.addEventListener("keydown",dirContral)
function dirContral(e) { 
	if(e.keyCode == 37 && dir!="right")
		dir = "left";
	else if(e.keyCode == 38 && dir!="down")
		dir = "up";
	else if(e.keyCode == 39 && dir!="left")
		dir = "right";
	else if(e.keyCode == 40 && dir!="up")
		dir = "down";
}

//create food
var food = {
	x:Math.round(Math.random()*(cvs.width/snakeW-1)),
	y:Math.round(Math.random()*(cvs.height/snakeH-1))
}

//draw food
function drawFood(x,y) { 
	ctx.fillStyle = "red"
	ctx.fillRect(x*snakeW,y*snakeH,snakeW,snakeH)
	ctx.fillStyle = "black"
	ctx.strokeRect(x*snakeW,y*snakeH,snakeW,snakeH)
}

//function describe game
function draw() {
	ctx.clearRect(0,0,cvs.width,cvs.height)
	snakeX++
	for(var i=0;i<snake.length;i++) {
		 draw_snake_shap(snake[i].x,snake[i].y)
	}
	drawFood(food.x,food.y)
	//snake head
	var snakeX = snake[0].x
	var snakeY = snake[0].y
	
	//Game over condition
	if(snakeX<0 || snakeY <0 || snakeX >=cvs.width/snakeW || snakeY >=cvs.height/snakeH)
		MyStopFunction(snake.length-4)
	for(var i =1;i<snake.length;i++)
		if(snake[0].x == snake[i].x && snake[0].y == snake[i].y )
			MyStopFunction(snake.length-4)
	//control function
	if(dir == "right") snakeX++;
	else if(dir=="left") snakeX--;
	else if(dir == "up")snakeY--;
	else if(dir == "down") snakeY++;
	
	if(snakeX == food.x && snakeY == food.y) {
		food = {
			x:Math.round(Math.random()*(cvs.width/snakeW-1)),
			y:Math.round(Math.random()*(cvs.height/snakeH-1))
		}		
		//create newHead
		var newHead = {
			x:snakeX ,
			y:snakeY
		}
	}
	else {
		snake.pop()
		//create newHead
		var newHead = {
			x:snakeX ,
			y:snakeY
		}
	}
	snake.unshift(newHead)
}
var myTime;
var interval;
function start() {
	var speed = document.getElementById("speed").value;
	var color = document.getElementById("color").value;
	if (color == "red")
		cvs.style.backgroundColor = "red";
	else if (color == "yellow")
		cvs.style.backgroundColor = "yellow";
	else if (color == "green")
		cvs.style.backgroundColor = "green";
	if(speed == "0")
		alert("Please select the level of game");
	else if (speed == "1")
		interval = 500;
	else if(speed == "2")
		interval = 400;
	if(speed == "3")
		interval = 300;
	else if (speed == "4")
		interval = 200;
	else if(speed == "5")
		interval = 100;
	myTime = setInterval(draw,interval);
}
function stop() {
		alert("game is stoped");
		clearInterval(myTime)
}
function MyStopFunction(value) {
		alert("game is over and your score is :"+value)
		clearInterval(myTime)
}